const mongoose = require('mongoose');

const Schema = mongoose.Schema;
var leaderSchema = new Schema({
    name:{
        type:String,
        required:true
    },
    image:{
        type:String,
        required:true
    },
    designation:{
        type:String,
        required:true
    },
    abbr:{
        type:String,
        required:true
    },
    description:{
        type:String,
        default: false 
    },
    featured:{
        type:Boolean,
        required:true   
    }
}, {
    timestamps: true
})
const Leader = mongoose.model('Leader',leaderSchema);
module.exports = Leader;