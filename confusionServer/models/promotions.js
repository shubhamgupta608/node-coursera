const mongoose = require('mongoose');
require('mongoose-currency').loadType(mongoose);
const Schema = mongoose.Schema;
const Currency = mongoose.Types.Currency;
var promotionSchema = new Schema({
    name:{
        type:String,
        required:true
    },
    image:{
        type:String,
        required:true
    },
    label:{
        type:String,
        required:true
    },
    price:{
        type:Currency,
        required:true
    },
    description:{
        type:String,
        required:true   
    }
}, {
    timestamps: true
})
const Promotion = mongoose.model('Promotion',promotionSchema);
module.exports = Promotion;